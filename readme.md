#uruchomienie

1. Poprawiamy ścieżki do ceryfikatów w pliku application.yaml
2. Odpalamy z Intelij SecureAppServerApplication.main() 
3. Następnie mozna testować 2 way ssl korzystając z klasy SecureAppServerApplicationTests

#import czesci publicznej certyfikatu do keystore
  **keytool -importcert -keystore <plik_certyfikatem_klienckim> -alias server_cert -file server-public.cer -storepass <haslo> -noprompt**
 
CA ROOT i inne ceryfikaty generowałem przy użyciu gooMint X.509 CA Manager



Budujemy przy użyciu 
./gradlew clean build -x test   
java8 