package com.security.server;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.swing.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.KeyStore;

import static org.junit.Assert.assertNotNull;


@RunWith(SpringRunner.class)
@SpringBootTest
public class SecureAppServerApplicationTests {


	public static final String URL = "https://localhost:8443/test/security/hello/Piotr";

	@Autowired
	private HttpConfig httpConfig;


	@Test
	public void executeWithSecuredClient() throws IOException {
		HttpClient client = httpConfig.getHttpClient();
		execute(client);
	}


	@Test
	public void executeNonSecuredClient() throws IOException {
		HttpClient client = httpConfig.getHttpClientIgnoreServerCheck();
		execute(client);
	}


	private String execute(HttpClient client ) throws IOException {

		RequestBuilder requestBuilder = RequestBuilder.get();
		requestBuilder.setUri(URL);

		HttpResponse proxyResponse = client.execute(requestBuilder.build());
		String res = getResponseAsString(proxyResponse);
		System.out.println("\n \n \nResponse From service " + res +"\n \n");
		return res;
	}

	private String getResponseAsString(HttpResponse proxyResponse) throws IOException {
		ByteArrayOutputStream result = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int length;
		while ((length = proxyResponse.getEntity().getContent().read(buffer)) != -1) {
			result.write(buffer, 0, length);
		}

		String res =  result.toString("UTF-8");
		if (res.contains("Access Denied")) {
			throw new RuntimeException(("Invalid CA name in certificate"));
		}
		return res;
	}

}
