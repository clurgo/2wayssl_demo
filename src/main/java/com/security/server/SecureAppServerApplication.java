package com.security.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.HttpsURLConnection;


@SpringBootApplication
@Import({HttpConfig.class})
public class SecureAppServerApplication  {

	public static void main(String[] args) {
			SpringApplication.run(SecureAppServerApplication.class, args);
	}

}
