package com.security.server;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.ssl.TrustStrategy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.net.ssl.SSLContext;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

@Configuration
@Slf4j
public class HttpConfig {



    @Value("${client.ssl.key-store}")
    public String keyStoreName;

    @Value("${client.ssl.key-store-password}")
    public String keyStorePassword;

    @Value("${client.ssl.key-password}")
    public String keyPassword;

    public CloseableHttpClient getClientWithoutSSL() {
        return HttpClients.createDefault();
    }

    public CloseableHttpClient getHttpClient() {
        try {

            SSLContext trustedSSLContext = SSLContexts
                    .custom()
                    //FIXME to contain real trust store
                    .loadTrustMaterial(readStore(),null)
                    .loadKeyMaterial(readStore(),keyPassword.toCharArray())
                    .build();

            return HttpClients.custom()
                    .setSSLContext(trustedSSLContext)
                    .setSSLHostnameVerifier( NoopHostnameVerifier.INSTANCE)
                    .build();
        } catch (Exception e) {
            throw new RuntimeException("Ooops Something bad has happened during http client creation. ");
        }
    }


    public CloseableHttpClient getHttpClientIgnoreServerCheck() {
        try {

            // do not use it unless really have to
            TrustStrategy trustAllServerCertificates = new TrustStrategy() {
                @Override
                public boolean isTrusted(X509Certificate[] chain,
                                         String authType) throws CertificateException {
                    return true;
                }
            };



            SSLContext trustedSSLContext = SSLContexts
                    .custom()
                    //FIXME to contain real trust store
                    .loadTrustMaterial(trustAllServerCertificates)
                    .build();

            return HttpClients.custom()
                    .setSSLContext(trustedSSLContext)
                    .setSSLHostnameVerifier( NoopHostnameVerifier.INSTANCE)
                    .build();
        } catch (Exception e) {
            throw new RuntimeException("Ooops Something bad has happened during http client creation. ");
        }
    }




    @SneakyThrows
    public KeyStore readStore() {
        InputStream keyStoreStream = this.getClass().getClassLoader().getResourceAsStream(keyStoreName);
        KeyStore keyStore = null;
        if (keyStoreStream == null) {
            log.debug("Reading client certificate for sso from file {}",keyStoreName);
            keyStoreStream = new FileInputStream(keyStoreName);

        }
        if (keyStoreStream != null) {
            keyStore = KeyStore.getInstance("JKS"); // or "PKCS12"
            keyStore.load(keyStoreStream, keyStorePassword.toCharArray());
            return keyStore;
        }
        throw new RuntimeException("Cannot load Client keystore");
    }

}
