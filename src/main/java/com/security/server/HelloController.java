/**
 * 
 */
package com.security.server;


import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Aman
 *
 */
@RestController
@RequestMapping("/test/security")
@Slf4j
public class HelloController {


	@GetMapping(value = "/hello/{name}")
	public String testHello(@PathVariable String name) {

		return "If you can see it it means you executed two way ssl and  " + name + " !";
	}

}
